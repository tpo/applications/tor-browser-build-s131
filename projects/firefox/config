# vim: filetype=yaml sw=2
version: '[% c("abbrev") %]'
filename: 'firefox-[% c("var/project-name") %]-[% c("version") %]-[% c("var/osname") %]-[% c("var/build_id") %]'
git_hash: '[% c("var/project-name") %]-[% c("var/firefox_version") %]-[% c("var/browser_branch") %]-build[% c("var/browser_build") %]'
tag_gpg_id: 1
git_url: https://gitlab.torproject.org/tpo/applications/tor-browser.git
gpg_keyring:
  - pierov.gpg
  - richard.gpg
container:
  use_container: 1

var:
  firefox_platform_version: 102.9.0
  firefox_version: '[% c("var/firefox_platform_version") %]esr'
  browser_series: '12.5'
  browser_branch: '[% c("var/browser_series") %]-1'
  browser_build: 2
  branding_directory_prefix: 'tb'
  copyright_year: '[% exec("git show -s --format=%ci").remove("-.*") %]'
  nightly_updates_publish_dir: '[% c("var/nightly_updates_publish_dir_prefix") %][% c("var/osname") %]'
  gitlab_project: https://gitlab.torproject.org/tpo/applications/tor-browser
  git_commit: '[% exec("git rev-parse HEAD") %]'
  deps:
    - build-essential
    - unzip
    - zip
    - autoconf2.13
    - yasm
    - pkg-config
  has_l10n: '[% !c("var/testbuild") && c("var/locales").size %]'

  rezip: |
    rezip_tmpdir=$(mktemp -d)
    mkdir -p "$rezip_tmpdir/z"
    unzip -q -d "$rezip_tmpdir/z" -- [% c("rezip_file") %] || [ $? -lt 3 ]
    pushd "$rezip_tmpdir/z"
    [% c("zip", {
      zip_src => [ '.' ],
      zip_args => '$rezip_tmpdir/new.zip',
    }) %]
    popd
    mv -f -- "$rezip_tmpdir/new.zip" [% c("rezip_file") %]
    rm -Rf "$rezip_tmpdir"

  l10n-changesets: '[% exec("cat browser/locales/l10n-changesets.json") %]'

steps:
  src-tarballs:
    filename: 'src-[% project %]-[% c("version") %].tar.xz'
    version: '[% c("git_hash") %]'
    input_files: []
    container:
      use_container: 0
    targets:
      nightly:
        version: '[% c("abbrev") %]'

  list_toolchain_updates:
    git_url: https://github.com/mozilla/gecko-dev.git
    git_hash: esr102
    tag_gpg_id: 0
    input_files: []
    container:
      use_container: 0

targets:
  basebrowser:
    # basebrowser tag always has a -build1 suffix
    git_hash: '[% c("var/project-name") %]-[% c("var/firefox_version") %]-[% c("var/browser_branch") %]-build1'
    var:
      nightly_updates_publish_dir_prefix: basebrowser-

  nightly:
    git_hash: '[% c("var/project-name") %]-[% c("var/firefox_version") %]-[% c("var/browser_branch") %]'
    tag_gpg_id: 0

  mullvadbrowser:
    git_url: git@gitlab.torproject.org:tpo/applications/privacy-browser.git
    var:
      branding_directory_prefix: 'mb'
      firefox_platform_version: 102.9.0
      browser_series: '12.0'
      browser_branch: '[% c("var/browser_series") %]-1'
      browser_build: 5
      gitlab_project: https://gitlab.torproject.org/tpo/applications/privacy-browser

  linux-x86_64:
    var:
      martools_filename: mar-tools-linux64.zip
      arch_deps:
        - libgtk2.0-dev
        - libgtk-3-dev
        - libdbus-glib-1-dev
        - libxt-dev
        - hardening-wrapper
        # To pass configure since ESR 31
        - libpulse-dev
        # To pass configure since ESR 52
        - libx11-xcb-dev
        # To pass configure since ESR 102
        - libasound2-dev
        # To support Wayland mode
        - libdrm-dev
      libdir: lib64

  linux-i686:
    var:
      martools_filename: mar-tools-linux32.zip
      sort_deps: 0
      arch_deps:
        - libgtk2.0-dev:i386
        - libgtk-3-dev:i386
        - libdbus-glib-1-dev:i386
        - libxt-dev:i386
        - hardening-wrapper
        # To pass configure since ESR 31
        - libpulse-dev:i386
        # To pass configure since ESR 52
        - libx11-xcb-dev:i386
        # To pass configure since ESR 102
        - libasound2-dev:i386
        # To support Wayland mode
        - libdrm-dev:i386
      libdir: lib32

  macos:
    var:
      martools_filename: 'mar-tools-macos-[% c("var/macos_arch") %].zip'
      nightly_updates_publish_dir: '[% c("var/nightly_updates_publish_dir_prefix") %]macos'
      arch_deps:
        - python3
        - python3-distutils
        - rsync

  windows:
    var:
      arch_deps:
        - python3
        - python3-distutils
        - wine

  windows-i686:
    var:
      martools_filename: mar-tools-win32.zip

  windows-x86_64:
    var:
      martools_filename: mar-tools-win64.zip

input_files:
  - project: container-image
  - name: '[% c("var/compiler") %]'
    project: '[% c("var/compiler") %]'
  - filename: get-moz-build-date
  - project: binutils
    name: binutils
    enable: '[% c("var/linux") %]'
  - filename: fix-info-plist.py
    enable: '[% c("var/macos") %]'
  - filename: nsis-uninstall.patch
    enable: '[% c("var/windows") %]'
  - project: rust
    name: rust
  - project: cbindgen
    name: cbindgen
  - project: firefox-l10n
    name: firefox-l10n
    enable: '[% c("var/has_l10n") %]'
  - project: wasi-sysroot
    name: wasi-sysroot
    enable: '[% c("var/rlbox") %]'
  - project: node
    name: node
  - project: nasm
    name: nasm
  - project: python
    name: python
    enable: '[% c("var/linux") %]'
  - project: clang
    name: clang
    enable: '[% c("var/linux") %]'
  - project: fxc2
    name: fxc2
    enable: '[% c("var/windows") %]'
    target_prepend:
      - torbrowser-windows-x86_64
  - filename: abicheck.cc
    enable: '[% c("var/linux") %]'
  - project: translation
    name: translation-base-browser
    pkg_type: base-browser
    enable: '[% c("var/has_l10n") %]'
  - project: translation
    name: translation-base-browser-fluent
    pkg_type: base-browser-fluent
    enable: '[% c("var/has_l10n") %]'
  - project: translation
    name: translation-tor-browser
    pkg_type: tor-browser
    enable: '[% c("var/tor-browser") && c("var/has_l10n") %]'
  - filename: namecoin-torbutton.patch
    enable: '[% c("var/namecoin") %]'
    # TorButton patch authored by Arthur Edelstein, from https://github.com/arthuredelstein/torbutton/ branch 2.1.10-namecoin
  - filename: namecoin-etld.patch
    enable: '[% c("var/namecoin") %]'
