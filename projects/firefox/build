#!/bin/bash
[% c("var/set_default_env") -%]
[% pc(c('var/compiler'), 'var/setup', { compiler_tarfile => c('input_files_by_name/' _ c('var/compiler')) }) %]
distdir=/var/tmp/dist/[% project %]
mkdir -p /var/tmp/build
mkdir -p [% dest_dir _ '/' _ c('filename') %]

[% IF c("var/windows") %]
  # Setting up fxc2
  tar -C /var/tmp/dist -xf [% c('input_files_by_name/fxc2') %]
  export PATH="/var/tmp/dist/fxc2/bin:$PATH"
[% END -%]

tar -C /var/tmp/dist -xf [% c('input_files_by_name/rust') %]
tar -C /var/tmp/dist -xf [% c('input_files_by_name/cbindgen') %]
tar -C /var/tmp/dist -xf [% c('input_files_by_name/nasm') %]
tar -C /var/tmp/dist -xf [% c('input_files_by_name/node') %]
export PATH="/var/tmp/dist/rust/bin:/var/tmp/dist/cbindgen:/var/tmp/dist/nasm/bin:/var/tmp/dist/node/bin:$PATH"

[% IF c("var/linux") %]
  tar -C /var/tmp/dist -xf [% c('input_files_by_name/clang') %]
  tar -C /var/tmp/dist -xf [% c('input_files_by_name/python') %]
  export PATH="/var/tmp/dist/python/bin:$PATH"
  tar -C /var/tmp/dist -xf $rootdir/[% c('input_files_by_name/binutils') %]
  export PATH="/var/tmp/dist/binutils/bin:$PATH"
  # Use clang for everything on Linux now if we don't build with ASan.
  [% IF ! c("var/asan") -%]
    export PATH="/var/tmp/dist/clang/bin:$PATH"
  [% END -%]
  [% IF c("var/linux-i686") %]
    # Exporting `PKG_CONFIG_PATH` in the mozconfig file is causing build
    # breakage in Rust code. It seems that environment variable is not passed
    # down properly in that case. Thus, we set it here in the build script.
    export PKG_CONFIG_PATH="${PKG_CONFIG_PATH}:/usr/lib/i386-linux-gnu/pkgconfig"
  [% END -%]
[% END -%]

[% IF c("var/rlbox") -%]
  tar -C /var/tmp/dist -xf [% c('input_files_by_name/wasi-sysroot') %]
  # XXX: We need the libclang_rt.builtins-wasm32.a in our clang lib directory.
  # Copy it over.
  # https://searchfox.org/mozilla-central/source/build/build-clang/build-clang.py#890,
  # include it directly in our clang
  [% IF c("var/macos") -%]
    rtdir=/var/tmp/dist/macosx-toolchain/clang/lib/clang/[% pc("clang", "version") %]/lib/wasi
  [% ELSIF c("var/windows") -%]
    rtdir=/var/tmp/dist/mingw-w64-clang/lib/clang/[% pc("clang", "version") %]/lib/wasi
  [% ELSE -%]
    rtdir=/var/tmp/dist/clang/lib/clang/[% pc("clang", "version") %]/lib/wasi
  [% END -%]
  mkdir -p $rtdir
  cp /var/tmp/dist/wasi-sysroot/lib/clang/*/lib/wasi/libclang_rt.builtins-wasm32.a $rtdir
  export WASI_SYSROOT=/var/tmp/dist/wasi-sysroot/share/wasi-sysroot
[% END -%]

tar -C /var/tmp/build -xf [% project %]-[% c('version') %].tar.gz

mkdir -p $distdir/[% IF ! c("var/macos") %]Browser[% END %]

cd /var/tmp/build/[% project %]-[% c("version") %]
cat > .mozconfig << 'MOZCONFIG_EOF'
[% INCLUDE mozconfig %]
MOZCONFIG_EOF
[% IF c("var/asan") -%]
  # Without disabling LSan our build is blowing up:
  # https://bugs.torproject.org/10599#comment:52
  export ASAN_OPTIONS="detect_leaks=0"
[% END -%]

eval $(perl $rootdir/get-moz-build-date [% c("var/copyright_year") %] [% c("var/torbrowser_version") %])
if [ -z $MOZ_BUILD_DATE ]
then
    echo "MOZ_BUILD_DATE is not set"
    exit 1
fi

[% IF c("var/windows") %]
  # Make sure widl is not inserting random timestamps, see #21837.
  export WIDL_TIME_OVERRIDE="0"
  patch -p1 < $rootdir/nsis-uninstall.patch
  # mingw-w64 does not support SEH on 32bit systems. Be explicit about that.
  export LDFLAGS="[% c('var/flag_noSEH') %]"
[% END -%]

[% IF c("var/namecoin") %]
  patch -p1 < $rootdir/namecoin-etld.patch
[% END -%]

[% IF c("var/namecoin") %]
  pushd toolkit/torbutton
  patch -p1 < $rootdir/namecoin-torbutton.patch
  popd
[% END %]

[% IF c("var/nightly") && ! c("var/mullvad-browser") -%]
  # Set update url for nightly (#33402 / #40033)
  sed -i 's|^URL=https://aus1\.torproject\.org/.*|URL=https://nightlies.tbb.torproject.org/nightly-updates/updates/nightly-[% c("var/nightly_updates_publish_dir") %]/%CHANNEL%/%BUILD_TARGET%/%VERSION%/ALL|' build/application.ini.in
[% END -%]


rm -f configure
rm -f js/src/configure

export MACH_BUILD_PYTHON_NATIVE_PACKAGE_SOURCE=system

# Create .mozbuild to avoid interactive prompt in configure
mkdir "$HOME/.mozbuild"

[% IF c("var/has_l10n") -%]
  supported_locales="[% tmpl(c('var/locales').join(' ')) %]"

  l10ncentral="$HOME/.mozbuild/l10n-central"
  mkdir "$l10ncentral"
  for tarball in $rootdir/[% c('input_files_by_name/firefox-l10n') %]/*; do
    tar -C "$l10ncentral" -xf "$tarball"
  done

  tar -C "$rootdir" -xf "$rootdir/[% c('input_files_by_name/translation-base-browser') %]"
  pushd "$rootdir/translation-base-browser"
  ln -s ja ja-JP-mac
  for lang in $supported_locales; do
    # Please notice that Fluent and DTDs use different directories in
    # l10n-central! This is something we have to keep in mind when we will join
    # the two branches!
    mv $lang/* "$l10ncentral/$lang/browser/chrome/browser/"
  done
  popd

  tar -C "$rootdir" -xf "$rootdir/[% c('input_files_by_name/translation-base-browser-fluent') %]"
  pushd "$rootdir/translation-base-browser-fluent"
  ln -s ja ja-JP-mac
  # TODO: These two sources will be unified eventually; at that point, this link
  # will go away, too.
  ln -s es es-ES
  for lang in $supported_locales; do
    mv $lang/languageNotification.ftl "$l10ncentral/$lang/browser/browser/"
  done
  popd

  [% IF c("var/tor-browser") -%]
    # We will have to keep the torbutton.jar until we stop using dtds, because
    # if we move them to the browser locale directory, browser.xhtml is not
    # loaded.
    tar -C "$rootdir" -xf "$rootdir/[% c('input_files_by_name/translation-tor-browser') %]"
    transl_tor_browser="$rootdir/translation-tor-browser"
    torbutton_locales="toolkit/torbutton/chrome/locale/"
    torbutton_jar="toolkit/torbutton/jar.mn"
    for lang in $supported_locales; do
      central_lang=$lang
      [% IF c("var/macos") -%]
        if [ "$lang" == "ja-JP-mac" ]; then
          lang="ja"
        fi
      [% END -%]
      mv "$transl_tor_browser/$lang/cryptoSafetyPrompt.properties" "$l10ncentral/$central_lang/browser/chrome/browser/"
      mv "$transl_tor_browser/$lang" "$torbutton_locales/"
      echo "% locale torbutton $lang %locale/$lang/" >> "$torbutton_jar"
      echo "    locale/$lang (chrome/locale/$lang/*)" >> "$torbutton_jar"
    done
  [% END -%]
[% ELSE -%]
  supported_locales=""
[% END -%]

# PyYAML tries to read files as ASCII, otherwise
export LC_ALL=C.UTF-8
export LANG=C.UTF-8

./mach configure \
  --with-distribution-id=org.torproject \
  --with-base-browser-version=[% c("var/torbrowser_version") %] \
  [% IF c("var/updater_enabled") -%]
        --enable-update-channel=[% c("var/channel") %] \
  [% END %] \
  [% IF !c("var/base-browser") -%]--with-branding=browser/branding/[% c("var/branding_directory_prefix") %]-[% c("var/channel") %][% END %] \
  [% IF !c("var/rlbox") -%]--without-wasm-sandboxed-libraries[% END %]

./mach build --verbose
[% IF c("var/has_l10n") %]
  export MOZ_CHROME_MULTILOCALE="$supported_locales"
  # No quotes on purpose, see https://firefox-source-docs.mozilla.org/build/buildsystem/locales.html#instructions-for-multi-locale-builds
  ./mach package-multi-locale --locales en-US $MOZ_CHROME_MULTILOCALE
  AB_CD=multi ./mach build stage-package
[% ELSE %]
  ./mach build stage-package
[% END %]

[% IF c("var/macos") %]
  cp -a obj-*/dist/[% c('var/exe_name') %]/* $distdir
  [% IF c("var/base-browser") -%]
    mv "$distdir/Firefox.app" "$distdir/[% c('var/Project_Name') %].app"
  [% END -%]
  # Remove firefox-bin (we don't use it, see ticket #10126)
  rm -f "$distdir/[% c('var/Project_Name') %].app/Contents/MacOS/[% c('var/exe_name') %]-bin"

  # Adjust the Info.plist file
  INFO_PLIST="$distdir/[% c('var/Project_Name') %].app/Contents/Info.plist"
  python3 $rootdir/fix-info-plist.py \
    "$INFO_PLIST" \
    '[% c("var/Project_Name") %]' \
    '[% c("var/torbrowser_version") %]' \
    '[% c("var/copyright_year") %]' \
    [% IF c("var/mullvad-browser") -%]'Mullvad, Tor Browser and Mozilla Developers'[% ELSE -%]'The Tor Project'[% END %]
[% END %]

[% IF c("var/linux") %]
  [% IF c("var/linux-x86_64") && !c("var/asan") %]
    cp obj-*/testing/geckodriver/x86_64-unknown-linux-gnu/release/geckodriver $distdir
  [% END %]
  cp -a obj-*/dist/[% c('var/exe_name') %]/* $distdir/Browser/
  # Remove firefox-bin (we don't use it, see ticket #10126)
  rm -f "$distdir/Browser/[% c('var/exe_name') %]-bin"
  # TODO: There goes FIPS-140.. We could upload these somewhere unique and
  # subsequent builds could test to see if they've been uploaded before...
  # But let's find out if it actually matters first..
  rm -f $distdir/Browser/*.chk
  # Replace $exe_name by a wrapper script (#25485)
  mv "$distdir/Browser/[% c('var/exe_name') %]" "$distdir/Browser/[% c('var/exe_name') %].real"
  cat > "$distdir/Browser/[% c('var/exe_name') %]" << 'RBM_TB_EOF'
[% INCLUDE 'start-firefox' -%]
RBM_TB_EOF
  chmod 755 "$distdir/Browser/[% c('var/exe_name') %]"
[% END %]

[% IF c("var/windows") %]
  cp -a obj-*/dist/[% c('var/exe_name') %]/* $distdir/Browser/
  [% IF c("var/windows-i686") %]
    cp -a /var/tmp/dist/fxc2/bin/d3dcompiler_47_32.dll $distdir/Browser/d3dcompiler_47.dll
  [% ELSE %]
    cp -a /var/tmp/dist/fxc2/bin/d3dcompiler_47.dll $distdir/Browser
  [% END %]
[% END %]

[% IF c("var/updater_enabled") -%]
  # Make MAR-based update tools available for use during the bundle phase.
  # Note that mar and mbsdiff are standalone tools, compiled for the build
  # host's architecture.  We also include signmar, certutil, and the libraries
  # they require; these utilities and libraries are built for the target
  # architecture.
  MARTOOLS=$distdir/mar-tools
  mkdir -p $MARTOOLS
  cp -p config/createprecomplete.py $MARTOOLS/
  cp -p tools/update-packaging/*.sh $MARTOOLS/
  cp -p obj-*/dist/host/bin/mar $MARTOOLS/
  cp -p obj-*/dist/host/bin/mbsdiff $MARTOOLS/
  [% IF c("var/linux") || c("var/macos") %]
    cp -p obj-*/dist/bin/signmar $MARTOOLS/
    cp -p obj-*/dist/bin/certutil $MARTOOLS/
    cp -p obj-*/dist/bin/modutil $MARTOOLS/
    cp -p obj-*/dist/bin/pk12util $MARTOOLS/
    cp -p obj-*/dist/bin/shlibsign $MARTOOLS/
    [% IF c("var/linux") %]
      NSS_LIBS="libfreeblpriv3.so libmozsqlite3.so libnss3.so libnssckbi.so libnssutil3.so libsmime3.so libsoftokn3.so libssl3.so"
      NSPR_LIBS="libnspr4.so libplc4.so libplds4.so"
    [% ELSE %]
      NSS_LIBS="libfreebl3.dylib libmozglue.dylib libnss3.dylib libnssckbi.dylib libsoftokn3.dylib"
      # No NSPR_LIBS for macOS
      NSPR_LIBS=""
    [% END %]
    for LIB in $NSS_LIBS $NSPR_LIBS; do
      cp -p obj-*/dist/bin/$LIB $MARTOOLS/
    done
  [% END %]
  [% IF c("var/windows") %]
    cp -p obj-*/dist/bin/signmar.exe $MARTOOLS/
    cp -p obj-*/dist/bin/certutil.exe $MARTOOLS/
    cp -p obj-*/dist/bin/modutil.exe $MARTOOLS/
    cp -p obj-*/dist/bin/pk12util.exe $MARTOOLS/
    cp -p obj-*/dist/bin/shlibsign.exe $MARTOOLS/
    NSS_LIBS="freebl3.dll mozglue.dll nss3.dll nssckbi.dll softokn3.dll"
    for LIB in $NSS_LIBS; do
        cp -p obj-*/dist/bin/$LIB $MARTOOLS/
    done
  [% END %]
[% END -%]

cd $distdir

[% IF c("var/linux-x86_64") %]
  [% IF !c("var/asan") %]
    # No need for an unstripped geckodriver
    strip geckodriver
  [% END %]
  mkdir -p $distdir/Debug/Browser
  # Strip and generate debuginfo for the firefox binary that we keep, all *.so
  # files, the plugin-container, and the updater (see ticket #10126)
  for LIB in Browser/*.so "Browser/[% c('var/exe_name') %].real" Browser/plugin-container [% IF c("var/updater_enabled") -%]Browser/updater[% END %]
  do
      objcopy --only-keep-debug $LIB Debug/$LIB
      strip $LIB
      objcopy --add-gnu-debuglink=./Debug/$LIB $LIB
  done
[% END %]

# Re-zipping the omni.ja files is not needed to make them reproductible,
# however if we don't re-zip them, the files become corrupt when we
# update them using 'zip' and firefox will silently fail to load some
# parts.
[% IF c("var/windows") || c("var/linux") %]
  [% c("var/rezip", { rezip_file => 'Browser/omni.ja' }) %]
  [% c("var/rezip", { rezip_file => 'Browser/browser/omni.ja' }) %]
[% ELSIF c("var/macos") %]
  [% c("var/rezip", { rezip_file => '"' _ c("var/Project_Name") _ '.app/Contents/Resources/omni.ja"' }) %]
  [% c("var/rezip", { rezip_file => '"' _ c("var/Project_Name") _ '.app/Contents/Resources/browser/omni.ja"' }) %]
[% END %]

[%
IF c("var/macos");
  SET browserdir='"' _ c("var/Project_Name") _ '.app/Contents"';
ELSE;
  SET browserdir='Browser';
END;
%]

[% IF c("var/linux") %]
  /var/tmp/dist/gcc/bin/g++ $rootdir/abicheck.cc -o Browser/abicheck -std=c++17
  [% IF !c("var/torbrowser") %]
    libdest=Browser/libstdc++
    mkdir -p "$libdest"
    # FIXME: tor-browser-build#40749
    cp /var/tmp/dist/gcc/[% c("var/libdir") %]/libstdc++.so.* "$libdest"
    [% IF c("var/asan") -%]
      cp /var/tmp/dist/gcc/[% c("var/libdir") %]/libasan.so.* "$libdest"
      cp /var/tmp/dist/gcc/[% c("var/libdir") %]/libubsan.so.* "$libdest"
    [% END -%]
  [% END %]
[% END %]

[% c('tar', {
        tar_src => [ browserdir ],
        tar_args => '-czf ' _ dest_dir _ '/' _ c('filename') _ '/browser.tar.gz',
    }) %]

[% IF c("var/linux-x86_64") %]
[% c('tar', {
        tar_src => [ 'Debug' ],
        tar_args => '-cJf ' _ dest_dir _ '/' _ c('filename') _ '/browser-debug.tar.xz',
    }) %]
  [% IF !c("var/asan") %]
    [% c('tar', {
            tar_src => [ 'geckodriver' ],
            tar_args => '-cJf ' _ dest_dir _ '/' _ c('filename') _ '/geckodriver-linux64.tar.xz',
        }) %]
  [% END %]
[% END %]

[% IF c("var/updater_enabled") -%]
  [% c('zip', {
          zip_src => [ 'mar-tools' ],
          zip_args => dest_dir _ '/' _ c('filename') _ '/' _ c('var/martools_filename'),
      }) %]
[% END -%]

[% IF c("var/build_infos_json") -%]
  cat > "[% dest_dir _ '/' _ c('filename') _ '/build-infos.json' %]" << EOF_BUILDINFOS
  {
      "firefox_platform_version" : "[% c("var/firefox_platform_version") %]",
      "firefox_buildid" : "$MOZ_BUILD_DATE"
  }
EOF_BUILDINFOS
[% END -%]
