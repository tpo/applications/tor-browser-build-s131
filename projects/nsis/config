# vim: filetype=yaml sw=2
version: 3.08
filename: 'nsis-[% c("version") %]-[% c("var/osname") %]-[% c("var/build_id") %].tar.gz'
container:
  use_container: 1
var:
  deps:
    - build-essential
    - libmpc-dev
    - scons
    - zlib1g-dev
    - libcppunit-dev
    - xsltproc
  # NSIS has an assembly part that cannot be compiled with Clang.
  # Mozilla uses -fno-integrated-as (see
  # taskcluster/scripts/misc/build-mingw32-nsis.sh) but for some reason this
  # does not seem to work for us, so just keep GCC for the moment, since we are
  # already using it for Rust anyway.
  compiler: mingw-w64

input_files:
  - project: container-image
  - filename: 'nsis-[% c("version") %].tar.bz2'
    URL: 'https://downloads.sourceforge.net/nsis/nsis-[% c("version") %]-src.tar.bz2'
    sha256sum: a85270ad5386182abecb2470e3d7e9bec9fe4efd95210b13551cb386830d1e87
  - filename: no-insert-timestamp.patch
  - filename: no-reloc-section.diff
  - name: '[% c("var/compiler") %]'
    project: '[% c("var/compiler") %]'
  - name: zlib
    project: zlib
